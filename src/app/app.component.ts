import { Component, HostListener, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent implements OnInit {
  title = 'portfolio';
  constructor(){}
  ngOnInit(): void {
  }
  

  @HostListener('window:load', ['$event'])
  onLoad() {
    // Agregar la clase fade-in después de que la página se haya cargado completamente
    const header = document.getElementById('appHeader');
    if (header) {
      header.classList.add('fade-in');
    }else{
      console.log("that header does not exist")
    }
    //Agregar clase show al ul h-list
    const showlist = document.getElementById('h-ul');
    if(showlist){
      showlist.classList.add('show')
    }else{
      console.log("that list does not exist")
    }
  }
}
